# my dotfiles for envs.net

this repo is my collection of dotfiles.

it includes configs for my most frequently used tools.<br />
steps to install on debian and derivatives

1. `apt install make stow git byobu nano vim zsh mutt gpg gpg-agent`
2. `make nuke install` - note that nuke removes existing files in your $HOME
