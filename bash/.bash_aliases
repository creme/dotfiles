#
# User specific aliases and functions
#
alias _='sudo'
alias sysu='systemctl --user'

alias edit="$EDITOR"
alias grep='grep --color'

alias update_git_dirs='for x in * ; do if [ -d $x ]; then cd $x ; git pull origin master ; cd .. ; fi; done'

# Set the section search order for overlapping names
alias man='man -S 2:3:1:4:5:6:7:8'

alias rm='rm --preserve-root'

alias ls='exa -ag'
alias la='exa -lag'
alias ll='exa -lahg'

alias h='history'
alias m='more'
alias psa='ps -axuwf'
alias psm="ps -axuwf | grep $USER | more"

# todo-txt
alias todo='todo-txt'
alias t='todo-txt'

# twtxt
alias twtxt_users="curl 'https://twtxt.envs.net/api/plain/users'"
twtxt_user() { curl 'https://twtxt.envs.net/api/plain/users?q='"$1" ; }
twtxt_user_url() { curl 'https://twtxt.envs.net/api/plain/users?url='"$1" ; }

alias twtxt_tweets="curl 'https://twtxt.envs.net/api/plain/tweets'"
twtxt_tweet() { curl 'https://twtxt.envs.net/api/plain/tweets?q='"$1" ; }

alias twtxt_tags="curl 'https://twtxt.envs.net/api/plain/tags'"
twtxt_tag() { curl 'https://twtxt.envs.net/api/plain/tags/'"$1" ; }

alias twtxt_mentions="curl 'https://twtxt.envs.net/api/plain/mentions'"
twtxt_mention_url() { curl 'https://twtxt.envs.net/api/plain/mentions?url='"$1" ; }

#
# envs.net
#
0file() { curl -F"file=@$1" https://envs.sh ; }
0pb() { curl -F'file=@-;' https://envs.sh ; }
0url() { curl -F"url=$1" https://envs.sh ; }
0short() { curl -F"shorten=$1" https://envs.sh ; }

#
# admin
#
alias f2b-list='sudo /sbin/ipset list blacklist_fail2ban'

alias services='sudo -iu services'

alias envs_media='sudo du -hs \
    /var/lib/lxc/0x0/rootfs/srv/0x0 \
    /var/lib/lxc/0x0/rootfs/srv/ffsync/syncserver.db \
    /var/lib/lxc/gitea/rootfs/srv/git ; \
	printf "#matrix\n" ; ssh root@srv01.envs.net "ssh envs-matrix du -hs /var/lib/postgresql /var/matrix-media" ; \
	printf "#pleroma\n" ; ssh root@srv01.envs.net "ssh envs-pleroma du -hs /var/lib/postgresql /var/lib/pleroma"
'

# mail
alias mail_queue='sudo lxc-attach -n mail -- postqueue -p'

alias mail_logins='sudo lxc-attach -n mail -- doveadm who | sort'
alias mail_logins_extend='sudo lxc-attach -n mail -- doveadm who -1 | sort'
alias mail_logins_count='sudo lxc-attach -n mail -- doveadm who -1 | sort | wc -l'

alias mail_log-info='sudo tail -f /var/lib/lxc/mail/rootfs/var/log/mail.info'
alias mail_log-warn='sudo tail -f /var/lib/lxc/mail/rootfs/var/log/mail.warn'
alias mail_log-err='sudo tail -f /var/lib/lxc/mail/rootfs/var/log/mail.err'

alias lists_log-info='sudo tail -f /var/lib/lxc/lists/rootfs/var/log/mail.info'
alias lists_log-warn='sudo tail -f /var/lib/lxc/lists/rootfs/var/log/mail.warn'
alias lists_log-err='sudo tail -f /var/lib/lxc/lists/rootfs/var/log/mail.err'

# LXC
alias lxc-create_custom='sudo bash /usr/local/bin/lxc-create.sh'

alias lxc-info_all='for i in $(sudo lxc-ls -1); do echo "###"; sudo lxc-info -n $i; done'
alias lxc-ls_full='sudo lxc-ls -1f -F NAME,STATE,AUTOSTART,PID,RAM,SWAP,GROUPS,IPV4,INTERFACE,IPV6'
alias qm-ls_full='ssh root@srv01.envs.net "pvesh get /cluster/resources --type vm"'

alias lxc-update-all='for i in $(sudo lxc-ls -1); do echo -e "\n### $i ###"; sudo lxc-attach -n $i -- bash -c "apt update -y ; apt dist-upgrade -y"; done'
alias lxc-update-fzf='for i in $(sudo lxc-ls -1); do echo -e "\n### $i ###"; sudo lxc-attach -n $i -- bash -c "cd /opt/fzf && ./install --key-bindings --completion --update-rc"; done'

alias lxc-start-all='for i in $(sudo lxc-ls -1); do echo -e "\n### $i ###"; sudo lxc-start -n $i; sleep 2 done'
alias lxc-stop-all='for i in $(sudo lxc-ls -1); do echo -e "\n### $i ###"; sudo lxc-stop -n $i; sleep 2 done'
alias lxc-reboot-all='for i in $(sudo lxc-ls -1); do echo -e "\n### $i ###"; sudo lxc-stop --reboot -n $i; sleep 2 done'

lxc-cmd-all() {
    if [ -n "$2" ]; then printf 'use: lxc-cmd-all "your command here.."\n' ;
    else
        for i in $(sudo lxc-ls -1); do echo -e "\n### $i ###"; sudo lxc-attach -n $i -- bash -c "$1"; done
    fi
}

# LOG
alias jwarn='sudo journalctl --system -x | grep warn'
alias jfail='sudo journalctl --system -x | grep fail'
alias jerr='sudo journalctl --system -x | grep error'
alias jdeni='sudo journalctl --system -x | grep denied'

#
# Stuff
#

alias clearswap='sudo swapoff -a && sudo swapon -a'

alias showip='printf "IP: %s\n" "$(curl -sL ip.envs.net)"'

alias speedtest='wget -O /dev/null http://90.130.70.73/10GB.zip --report-speed=bits'

alias weather='curl http://wttr.in'
alias weather_graph='curl http://v2.wttr.in'

alias starwars='nc towel.blinkenlights.nl 23'

alias mcguyvermini='{ sleep 1; printf "gemini://envs.net/\r\n" ; } | gnutls-cli --insecure envs.net:1965 | awk "/^[0-9][0-9] .+/{hot=1}hot"'

duh() {
  if [ "$#" -ne 1 ]; then
    du -hd1 . | sort -h;
  else
    du -h $@ | sort -h;
  fi
}
