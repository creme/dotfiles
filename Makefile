HOME = /home/creme

YELLOW = $$(tput setaf 226)
GREEN = $$(tput setaf 46)
RED = $$(tput setaf 196)
RESET = $$(tput sgr0)


install:
	@make bash zsh byobu config defaults bin local bbj finger fzf git gnupg nano mutt misc

uninistall:
	@make clean
clean:
	@printf "$(RED)--- clean -----------------------------------------------\n$(RESET)"
	stow -t "$(HOME)" -D bash
	stow -t "$(HOME)" -D zsh
	stow -t "$(HOME)" -D zsh_themes
	stow -t "$(HOME)" -D zsh_plugins
	stow -t "$(HOME)" -D byobu
	stow -t "$(HOME)" -D config
	stow -t "$(HOME)" -D defaults

	stow -t "$(HOME)" -D bin

	stow -t "$(HOME)" -D bbj
	stow -t "$(HOME)" -D finger
	stow -t "$(HOME)" -D fzf
	stow -t "$(HOME)" -D git
	stow -t "$(HOME)" -D gnupg
	stow -t "$(HOME)" -D nano
	stow -t "$(HOME)" -D mutt


bash:
	@printf "$(YELLOW)--- bash -----------------------------------------------\n$(RESET)"
	git submodule update --remote --init -- bash/.bash-git-prompt
	stow -t "$(HOME)" bash

zsh:
	@printf "$(YELLOW)--- zsh ------------------------------------------------\n$(RESET)"
	git submodule update --remote --init -- zsh/.oh-my-zsh
	stow -t "$(HOME)" zsh

	stow -t "$(HOME)/.oh-my-zsh/custom/themes/" zsh_themes

	git submodule update --remote --init -- zsh_plugins/zsh-autosuggestions
	git submodule update --remote --init -- zsh_plugins/zsh-syntax-highlighting
	stow -t "$(HOME)/.oh-my-zsh/custom/plugins/" zsh_plugins

byobu:
	@printf "$(YELLOW)--- byobu ----------------------------------------------\n$(RESET)"
	mkdir -p "$(HOME)"/.byobu
	stow -t "$(HOME)/.byobu" byobu

config:
	@printf "$(YELLOW)--- config ---------------------------------------------\n$(RESET)"
	mkdir -p "$(HOME)"/.config
	stow -t "$(HOME)/.config" config

defaults:
	@printf "$(YELLOW)--- defaults -------------------------------------------\n$(RESET)"
	stow -t "$(HOME)" defaults


bin:
	@printf "$(YELLOW)--- bin ------------------------------------------------\n$(RESET)"
	mkdir -p "$(HOME)"/bin
	stow -t "$(HOME)/bin" bin

local:
	@printf "$(YELLOW)--- local ----------------------------------------------\n$(RESET)"
	mkdir -p "$(HOME)"/local
	chmod 700 "$(HOME)"/local
	mkdir -p "$(HOME)"/local/workspace

bbj:
	@printf "$(YELLOW)--- bbj ------------------------------------------------\n$(RESET)"
	stow -t "$(HOME)" bbj

finger:
	@printf "$(YELLOW)--- finger ---------------------------------------------\n$(RESET)"
	stow -t "$(HOME)" finger

fzf:
	@printf "$(YELLOW)--- fzf ------------------------------------------------\n$(RESET)"
	git submodule update --remote --init -- fzf/.fzf
	stow -t "$(HOME)" fzf
	"$(HOME)"/.fzf/install --key-bindings --completion --update-rc

git:
	@printf "$(YELLOW)--- git ------------------------------------------------\n$(RESET)"
	stow -t "$(HOME)" git

gnupg:
	@printf "$(YELLOW)--- gnupg ----------------------------------------------\n$(RESET)"
	mkdir -p "$(HOME)/.gnupg"
	chmod 700 "$(HOME)/.gnupg"
	stow -t "$(HOME)" gnupg

nano:
	@printf "$(YELLOW)--- nano -----------------------------------------------\n$(RESET)"
	stow -t "$(HOME)" nano

mutt:
	@printf "$(YELLOW)--- mutt -----------------------------------------------\n$(RESET)"
	mkdir -p "$(HOME)/.mutt"
	chmod 700 "$(HOME)/.mutt"
	stow -t "$(HOME)" mutt

misc:
	@printf "$(YELLOW)--- misc -----------------------------------------------\n$(RESET)"
	mkdir -p "$(HOME)"/tmp


nuke:
	@printf "$(RED)--- nuking existing files ---------------------------------\n$(RESET)"
	rm -rf "$(HOME)"/.profile "$(HOME)"/.bash* "$(HOME)"/.aliases "$(HOME)"/.zshrc* "$(HOME)"/.oh-my-zsh "$(HOME)"/.fzf*
	rm -rf "$(HOME)"/.byobu "$(HOME)"/.ident "$(HOME)"/.gpg.rc "$(HOME)"/.gnupg
	rm -rf "$(HOME)"/.nanorc "$(HOME)"/.selected_editor "$(HOME)"/.tz "$(HOME)"/.plan "$(HOME)"/.project "$(HOME)"/.envs
	rm -rf "$(HOME)"/.mutt* "$(HOME)"/.gitconfig "$(HOME)"/.gitignore_global "$(HOME)"/.bbj*
	rm -rf "$(HOME)"/.config/burrow "$(HOME)"/.config/htop "$(HOME)"/.config/ranger "$(HOME)"/.config/micro "$(HOME)"/.config/systemd "$(HOME)"/.config/twtxt


.PHONY: install clean uninistall nuke bash zsh byobu config defaults bin local bbj finger fzf git gnupg nano mutt misc
